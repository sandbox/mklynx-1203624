<?php
// $Id$

/** @file
 *
 */

/**
 * Load a single record.
 *
 * @param $id
 *    The id representing the record we want to load.
 */
function zazzle_load($id, $reset = FALSE) {

  return zazzle_load_multiple(array($id), $reset);
}

/**
 * This function returns a array of product records and has as input a array of AID numbers.
 */
function zazzle_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {

  return entity_load('zz_products', $ids, $conditions, $reset);
}

/**
 * This function saves a product array of information to the database table.
 * If the record already exists it will be over written. This is where the fields AID and CREATED are filled.
 */
function zazzle_save($product) {

  print "<br />";
  print $product->productid;
  print "<br />";

  $result = db_query("SELECT aid FROM {zz_products} WHERE productid=:proid", array(':proid' => $product->productid,
    ));
  $sid = $result->fetchField(0);
  print $sid;
  print "<br />";
  print $test[$sid]->productid;
  print "<br />";
  $product->created = REQUEST_TIME;
  $product->aid     = $sid;
  $test             = zazzle_load($sid);
  if ($test[$sid]->productid = $product->productid) {
    entity_delete('zz_products', $sid);
  }
  entity_save('zz_products', $product);
  return $products->aid;
}

/**
 * This function returns a product array. Below is the mapping table for the input of this function.
 */
function container_zzproducts($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p) {


  $container = array(
    'aid' => '',
    'title' => $a,
    'description' => $b,
    'popnum' => $c,
    'imageurl' => $d,
    'productid' => $e,
    'price' => $f,
    'pubdate' => $g,
    'artist' => $h,
    'sales_link' => $i,
    'color' => $j,
    'size' => $k,
    'ptype' => $l,
    'kwords' => $m,
    'custom' => $n,
    'style' => $o,
    'prodnumber' => $p,
    'created' => '',
  );
  $test = entity_create('zz_products', $container);

  return $test;
}

function get_all_zazzle($store) {



  $ass_id = variable_get('zazzle_associate_id', '');

  echo date("H:m:s");
  echo "<br />";

  $rss              = new lastRSS;
  $rss->CDATA       = 'content';
  $rss->items_limit = 0;
  $perpage          = 100;
  $popnum           = 0;
  $pagenum          = 0;

  $rssurl = "http://feed.zazzle.com/" . $store . "/feed?st=popularity&at=" . $ass_id . "&isz=400&src=zstore&pg=1&cg&ps=1&ft=gb&opensearch=true";

  if ($rs = $rss->get($rssurl)) {
    print $rs;
    $totalnum = $rs['opensearch:totalResults'];

    $totalpage = ceil($totalnum / $perpage);
  }
  // fatal error - no cached RSS, no socket
  else {}

  // test values
  $perpage = 100;
  $totalpage = 100;
  print $perpage;
  print $totalpage;
  print $pagenum;
  // start while
  while ($pagenum < $totalpage) {

    // create new RSS URL
    $pagenum = $pagenum + 1;


    $rssurl = "http://feed.zazzle.com/" . $store . "/feed?st=popularity&at=" . $ass_id . "&isz=400&bg=10101&src=zstore&pg=$pagenum&cg&ps=$perpage&ft=gb&opensearch=true";

    // Get our RSS data
    // if from rss getting
    if ($rs = $rss->get($rssurl)) {

      // Iterate through the RSS items and extract their data
      foreach ($rs as $key => $val) {
        // we only care about the <item> nodes in this case
        if ($key == "items") {
          foreach ($val as $index => $value) {
            // getting the data in variables
            $title       = urldecode($value['title']);
            $description = urldecode($value['description']);
            $imageurl    = $value['g:image_link'];
            $productid   = $value['g:id'];
            $price       = $value['g:price'];
            $pubdate     = $value['pubDate'];
            $artist      = $value['artist'];
            $link        = $value['link'];
            $color       = $value['g:color'];
            $size        = $value['g:size'];
            $ptype       = $value['g:product_type'];
            $kwords      = $value['c:keywords'];
            $cust        = $value['c:iscustomizable'];
            $style       = $value['g:style'];

            // extra work on variables
            $prodnum = substr($productid, 0, 3);
            $pubdate = substr($pubdate, 0, -4);
            $pubdate = strtotime($pubdate);
            $pubdate = date('o-m-d H:m:s', $pubdate);
            $popnum  = $popnum + 1;
            print "$popnum===";
            switch ($prodnum) {
              case 128:
                $ptype = "bumper sticker";
                break;

              case 137:
                $ptype = "card-Greeting-Note";
                break;

              case 153:
                $ptype = "photo sculpture";
                break;

              case 155:
                $ptype = "Pet Clothing";
                break;

              case 156:
                $ptype = "photo print";
                break;

              case 167:
                $ptype = "keds shoe";
                break;

              case 172:
                $ptype = "US Postage";
                break;

              case 176:
                $ptype = "iPad/iPhone Case";
                break;

              case 228:
                $ptype = "Poster";
                break;

              case 235:
                $ptype = "t-shirt";
                break;

              case 240:
                $ptype = "Business Cards";
                break;

              case 243:
                $ptype = "Photo Cards";
                break;

              case 245:
                $ptype = "rack card";
                break;
            }

            // strip any html from the description
            $description = preg_replace("/<[^>]+>/", '', $description);
            $title = preg_replace("/<[^>]+>/", '', $title);

            $product = container_zzproducts(
              $title,
              $description,
              $popnum,
              $imageurl,
              $productid,
              $price,
              $pubdate,
              $artist,
              $link,
              $color,
              $size,
              $ptype,
              $kwords,
              $cust,
              $style,
              $prodnum
            );

            // write node to database

            zazzle_save($product);

            print "<br />";
          }
          // end for each index / value
        }
        // end if item
      }
      // end foreach $rs
    }
    //end if from rss getting
    // fatal error - no cached RSS, no socket
    else {}
    // end else from rss getting
  }
  // end while
  echo date("H:m:s");
}

